#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass beamer
\begin_preamble
\usetheme{Warsaw}
% or ...

\setbeamercovered{transparent}
% or whatever (possibly just delete it)
\end_preamble
\options 8pt
\use_default_options false
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman times
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 10
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 2
\use_package amssymb 2
\use_package cancel 0
\use_package esint 0
\use_package mathdots 1
\use_package mathtools 0
\use_package mhchem 1
\use_package stackrel 0
\use_package stmaryrd 0
\use_package undertilde 0
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 2
\tocdepth 2
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title

\lang polish
Wprowadzenie
\lang english
 do 
\lang polish
Bitcoin
\end_layout

\begin_layout Author
Michał Wielgosz, Marcin Gajewski
\end_layout

\begin_layout Institute
Uniwersytet Jagielloński
\end_layout

\begin_layout Date
Seminarium Kryptografia, marzec 2014
\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Spis treści
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\end_deeper
\begin_layout Section
Czym jest Bitcoin?
\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Czym jest Bitcoin? [Marcin]
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Protokół wymyślony przez Satoshi Nakamoto
\end_layout

\begin_layout Itemize
System płatności wykorzystujący technologię p2p, kryptowaluta będąca otwartoźród
łowym oprogramowaniem zaprojektowanym w 2009 roku
\end_layout

\begin_layout Itemize
Jest dla finansów tym samym czym e-mail dla tradycyjnej poczty
\end_layout

\begin_layout Itemize
Bitcoin nie jest kontrolowany przez jakąkolowiek firmę czy rząd
\end_layout

\end_deeper
\begin_layout Section
Podstawowe informacje o BTC
\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Podstawowe informacje o BTC.[Michał]
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Bitcoin (często używany skrót - BTC) jest zdecentralizowaną walutą.
 Oznacza to, że nie ma określonego emitenta.
\end_layout

\begin_layout Itemize
Baza danych Bitcoin (transakcje) jest rozproszona pomiędzy użytkownikami
 waluty.
\end_layout

\begin_layout Itemize
Kryptografia jest wykorzystywana, by uniemożliwić wycofywanie środków z
 portfela przez osoby niepowołane a także do zabezpieczania przed zakłamaniami
 zapisów w łańcuchu bloków.
\end_layout

\begin_layout Itemize
1 BTC można podzielić na 100 000 000 jednostek (zwanych satoshi) tzn.
 najmniejszą wartość jaką można posiadać to 0,0000001 BTC (inaczej niż w
 przypadku PLN, 1 gr = 0,01 zł).
\end_layout

\begin_layout Itemize
Do adresów Bitcoin nie są przypisywane żadne dane osobiste, w związku z
 tym można założyć, że posiadanie BTC oraz przelewy są anonimowe.
\end_layout

\begin_layout Itemize
Gwarantuje niezmienność zasad działania w czasie
\end_layout

\end_deeper
\begin_layout Section
Dlaczego powstał Bitcoin?
\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Dlaczego powstał Bitcoin?[Marcin]
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Współczesna bankowość jest technologiczcnie przestarzała i niewygodna
\end_layout

\begin_layout Itemize
Próba zastąpienia systemu rezerw częściowych
\end_layout

\begin_layout Itemize
Reakcja na coraz większą kontrolę i inwigilację naszego życia przez rządy
 i korporacje
\end_layout

\begin_layout Itemize
Alternatywa dla złota
\end_layout

\begin_layout Description
Największe problemy tradycyjnego systemu walut:
\end_layout

\begin_layout Itemize
Brak anonimowości
\end_layout

\begin_layout Itemize
Ograniczenia geograficzne i polityczne
\end_layout

\begin_layout Itemize
Szybkość i koszty transakcji (weekendy)
\end_layout

\begin_layout Itemize
Problemy z bezpieczeństwem (PayPass)
\end_layout

\begin_layout Itemize
Blokady finansowe (WikiLeaks)
\end_layout

\begin_layout Itemize
Kryzysy finansowe i inflacja
\end_layout

\end_deeper
\begin_layout Section
Jak to działa?
\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Jak zacząć?[Michał]
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout FrameSubtitle
Krótki opis działania
\end_layout

\begin_layout Standard
Nowy użytkownik do wykorzystywania dobrodziejstw Bitcoina potrzebuje jedynie
 portfela.
\end_layout

\begin_layout Standard
Po instalacji zostanie wygenerowany adres, który można przekazać innym osobom
 byśmy mogli dostawać przelewy na niego.
\end_layout

\begin_layout Standard
Można posiadać wiele adresów w portfelu.
\end_layout

\begin_layout Standard
Portfel służy też do wykonywania przelewów.
\end_layout

\end_deeper
\begin_layout Subsection*
Najpopularniejsze portfele
\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Najpopularniejsze portfele[Michał]
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Istnieje wiele programów, które możemy wykorzystywać jako portfele.
 Przykłady:
\end_layout

\begin_layout Itemize
\begin_inset CommandInset href
LatexCommand href
name "Bitcoin-Qt"
target "http://bitcoin.org/pl/pobieranie"

\end_inset

- oryginalny portfel, najbezpieczniejszy
\end_layout

\begin_layout Itemize
\begin_inset CommandInset href
LatexCommand href
name "Multibit"
target "https://multibit.org/"

\end_inset

 - lekki, polecany dla początkujących
\end_layout

\begin_layout Itemize
\begin_inset CommandInset href
LatexCommand href
name "Electrum"
target "https://electrum.org/"

\end_inset

 - lekki, nastawiony na szybkość
\end_layout

\begin_layout Standard
Powyższe oprogramowanie jest całkowicie otwartoźródłowe.
 Wszystkie działają na najpopularniejszych systemach operacyjnych (Windows,
 Linux, OS X).
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Standard
Oprócz portfeli, które można zainstalować na własnym komputerze istnieją
 też portfele przenośne i internetowe.
 Natychmiastowy dostęp do środków obarczony jest jednak ryzykiem.
 Nie są zalecane jeśli cenimy bezpieczeństwo gdyż łatwiej można stracić
 środki tam przechowywane (np.
 w wyniku kradzieży, zakończeniu prowadzenia portfela przez administratora
 itp).
\end_layout

\end_deeper
\begin_layout Subsection*
Bezpieczeństwo
\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Bezpieczeństwo[Marcin]
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Tak jak w realnym świecie należy pamiętać o zabezpieczeniu swojego portfela.
\end_layout

\begin_layout Itemize
Portfele internetowe
\end_layout

\begin_deeper
\begin_layout Itemize
można porównać do banków.
 Należy bardzo ostrożnie wybierać takie serwisy gdyż powierzymy własne bitcoiny
 komuś obcemu.
 Warto też używać dwuskładnikowe uwierzytelnianie jeśli administrator udostępnia
 taką możliwość.
\end_layout

\end_deeper
\begin_layout Itemize
Kopie bezpieczeństwa
\end_layout

\begin_deeper
\begin_layout Itemize
portfele umożliwiają w bardzo prosty sposób tworzenie kopii.
 Jeśli taka kopia ma być przechowywana na zdalnym serwerze należy bezwzględnie
 ją zaszyfrować.
\end_layout

\end_deeper
\begin_layout Itemize
Szyfrowanie
\end_layout

\begin_deeper
\begin_layout Itemize
portfele udostępniają też możliwość wprowadzania hasła przy uwierzytelnianiu
 transakcji.
 Zaleca się używanie silnych, niesłownikowych haseł.
 Należy też pamiętać, że zapomnienie hasła to nieodwracalna utrata środków
 z portfela.
\end_layout

\end_deeper
\end_deeper
\begin_layout Separator

\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Dodatkowe zabezpieczenia[Marcin]
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Portfel offline
\end_layout

\begin_deeper
\begin_layout Itemize
przechowywanie portfela w miejscu odłączonym od sieci.
 Znacznie utrudni potencjalną kradzież przez nieetycznych hakerów oraz zabezpiec
zy portfel przed awariami sprzętu.
\end_layout

\end_deeper
\begin_layout Itemize
Testament
\end_layout

\begin_deeper
\begin_layout Itemize
jeśli po śmierci informacje o ulokowaniu portfela oraz hasła nie zostaną
 w żaden sposób przekazane rodzinie bitcoiny z portfela zostaną na zawsze
 utracone.
\end_layout

\end_deeper
\end_deeper
\begin_layout Section
Specyfikacja
\end_layout

\begin_layout Subsection*
Adresy
\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Adresy
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Każdy użytkownik Bitcoin ma portfel, który może posiadać wiele adresów.
 Adresy BTC to klucze publiczne funkcji ECDSA (kryptografia krzywych eliptycznyc
h), skrócone za pomocą funkcji RIPEMD-160 i zakodowane w Base58.
 W związku z tym składają się z około 33 znaków rozpoczynając się od '1'
 lub '3'.
 Służą one do adresowania płatności.
 Często do 
\begin_inset Quotes eld
\end_inset

identyfikowania
\begin_inset Quotes erd
\end_inset

 przelewów tworzy się osobne adresy dla osobnych transakcji.
\end_layout

\end_deeper
\begin_layout Subsection*
Łańcuchy bloków
\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Łańcuchy bloków
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Łańcuch bloków jest współdzielonym zapisem transakcji na którym polega cała
 sieć Bitcoin.
 Wszystkie potwierdzone transakcje są w nim zapisane bez żadnych wyjątków.
 W ten sposób transakcje mogą zostać zweryfikowane pod kątem posiadania
 odpowiedniej ilości bitcoin na koncie wydającego.
 Integralność oraz porządek chronologiczny łańcucha bloków jest wymuszony
 poprzez kryptografię.
\end_layout

\begin_layout Standard
Aby zapobiec 
\begin_inset Quotes eld
\end_inset

podwójnemu
\begin_inset Quotes erd
\end_inset

 wydawaniu środków stosuje się mechanizm dowodu pracy (Proof of Work).
 Odbywa się przez kopaczy wykorzystujących moc swoich komputerów do obliczania
 haszy.
 Pokrótce wygląda to tak:
\end_layout

\end_deeper
\begin_layout Subsection*
Transakcje
\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Transakcje
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Transakcja to przesłanie pewnej wartości pomiędzy adresami Bitcoin która
 zostanie następnie zapisana łańcuchu bloków.
 Portfele Bitcoin zawierają część danych tajnych zwaną kluczem prywatnym
 dla każdego adresu Bitcoin.
 Klucze prywatne są używane do podpisywania zleceń gdyż niosą ze sobą matematycz
ne dane potwierdzające pochodzenie od właściciela danego adresu.
 Podpis zapobiega również modyfikacjom przez osoby trzecie na każdym etapie
 od momentu jej wydania.
 Wszystkie transakcje są rozpropagowane pomiędzy użytkownikami i potwierdzone
 przez sieć w kolejnych minutach w procesie zwanym wydobyciem.
\end_layout

\begin_layout Standard
Bitcoiny zawierają adres swojego posiadacza.
 Z tego powodu transakcje w Bitcoin działają w następujący sposób:
\end_layout

\begin_layout Standard
Łańcuch bloków zawiera kompletną historię od emitenta po obecnego posiadacza
 dlatego próba powtórnego przelania wykorzystanych BTC zostanie odrzucona.
\end_layout

\begin_layout Standard
Do przyśpieszania transakcji używa się systemu prowizji.
 Wprowadza się to, by zachęcić kopaczy do dalszego przeznaczania przez nich
 mocy obliczeniowej pomimo tego, że trudność wydobycia rośnie a nagroda
 za blok spada.
 Stosuje się to też przy bardzo małych transakcjach by zniechęcić spamerów.
\end_layout

\begin_layout Standard
Transakcje są nieodwracalne.
\end_layout

\end_deeper
\begin_layout Section
Wydobycie bitcoinów
\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Wydobycie bitcoinów
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Wydobywaniem BTC może zajmować się każdy użytkownik Bitcoin.
\end_layout

\begin_layout Itemize
By zwiększyć szansę na nagrodę użytkownicy nie kopią w pojedynkę, zwykle
 dołączają do jakiejś większej kopalni, która dzieli nagrodę proporcjonalnie
 do wniesionej mocy.
\end_layout

\begin_layout Itemize
Specyfika kopania bitcoinów sprawia, że bardziej opłaca się wykorzystywać
 moc kart graficznych niż procesorów.
\end_layout

\begin_layout Itemize
Powstały też specjalne urządzenia do kopania BTC: ASIC.
\end_layout

\begin_layout Itemize
Najpopularniejsze programy do kopania BTC:
\end_layout

\begin_deeper
\begin_layout Itemize
cgminer
\end_layout

\begin_layout Itemize
bfgminer
\end_layout

\begin_layout Itemize
minerd
\end_layout

\end_deeper
\begin_layout Itemize
Obecnie kopanie na domowym komputerze przestało się opłacać.
\end_layout

\begin_layout Itemize
Powstały też giełdy sprzedające moc do kopania kryptowalut (np.: https://cex.io/)
 oraz usługi w chmurze wyspecjalizowane w tym celu.
\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Nagradzanie kopaczy
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Na początku nagroda za znalezienie poprawnego rozwiązania była ustalona
 na 50 BTC.
 Co 4 lata jest zmniejszana o połowę (obecnie wynosi 25 BTC).
 Przewiduje się, że nagroda wyzeruje się w 2140 roku.
\end_layout

\begin_layout Standard
Aby nie zniechęcić kopaczy przed weryfikowanienem po wyzerowaniu nagrody
 wprowadza się prowizje transakcyjne, które trafiają do kopaczy.
\end_layout

\end_deeper
\begin_layout Section
RYSUNEK NA TABLICY
\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
RYSUNEK
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Enumerate
Alicja wysyła BTC do Boba
\end_layout

\begin_deeper
\begin_layout Enumerate
Alicja dodaje adres Boba do monet
\end_layout

\begin_deeper
\begin_layout Enumerate
Alicja potwierdza tę operację swoim kluczem prywatnym
\end_layout

\begin_layout Enumerate
Alicja wysyła informację o transakcji do partnerów p2p.
\end_layout

\begin_layout Enumerate
Pozostali uczestnicy sprawdzają poprawność transakcji.
\end_layout

\begin_layout Enumerate
Po zaakceptowaniu transakcji przez określoną liczbę użytkowników (najczęściej
 6) środki lądują w portfelu u Boba.
\end_layout

\end_deeper
\begin_layout Enumerate
niepotwierdzone transakcje trafiają do kolejnego kandydata na blok (otwieranie
 nowych bloków następuje co około 10 minut)
\end_layout

\begin_deeper
\begin_layout Enumerate
następuje wykonywanie skomplikowanych operacji matematycznych w celu znalezienia
 prawidłowego hasza
\end_layout

\begin_layout Enumerate
po znalezieniu rozwiązania zostaje ono ogłoszone sieci
\end_layout

\begin_layout Enumerate
następnie zostaje sprawdzona jego poprawność przez pozostałych użytkowników
 i ostatecznie dodane do łańcucha
\end_layout

\end_deeper
\begin_layout Standard
Proces ten zapewnia chronologiczność oraz chroni neutralność sieci.
 Każdy kolejny blok zawiera informację o poprzednim bloku także trudno jest
 sfałszować pojedyczny blok (sfałszowanie jednego unieważniłoby wszystkie
 poprzednie).
\end_layout

\end_deeper
\end_deeper
\begin_layout Section
Ekonomia
\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Ekonomia
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Bitcoiny oraz inne kryptowaluty są coraz powszechniej używane do płatności,
 szczególnie za usługi dostępne przez Internet.
\end_layout

\begin_layout Itemize
Ilość BTC jest ustalona sztywno na 21 milionów co oznacza, że inflacja w
 tym przypadku praktycznie nie istnieje.
\end_layout

\begin_layout Itemize
Wartość BTC jest bardzo niestabilna względem konwencjonalnych walut.
 Powodem jest młody wiek kryptowaluty, nowatorstwo oraz płynność rynków.
 W związku z tym należy być bardzo ostrożnym i rozważnym kupując tę walutę
 oraz mieć cały czas na uwadze nieprzewidywalne wahania kursów.
\end_layout

\begin_layout Itemize
W perspektywie długoterminowej w związku z przewidywanym wykopaniem wszystkich
 BTC do 2140 roku (podaż przestanie rosnąć) będzie zauważalna deflacja waluty.
\end_layout

\end_deeper
\begin_layout Section
Giełdy
\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Giełdy
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
BTC można zdobyć też poprzez zakup na specjalnej giełdzie.
 Najpopularniejsza polska giełda to Bitcurex (https://pln.bitcurex.com/) -
 9 miejsce na świecie pod względem wolumenu.
\end_layout

\begin_layout Standard
Najpopularniejsze giełdy zagraniczne:
\end_layout

\begin_layout Itemize
\begin_inset CommandInset href
LatexCommand href
name "btc-e"
target "https://btc-e.com/"

\end_inset


\end_layout

\begin_layout Itemize
\begin_inset CommandInset href
LatexCommand href
name "BitStamp"
target "https://www.bitstamp.net/"

\end_inset


\end_layout

\begin_layout Itemize
\begin_inset CommandInset href
LatexCommand href
name "BTC China"
target "https://btcchina.com/"

\end_inset


\end_layout

\begin_layout Itemize
\begin_inset CommandInset href
LatexCommand href
name "bitcoin.de"
target "https://www.bitcoin.de/"

\end_inset


\end_layout

\end_deeper
\begin_layout Section
Status prawny
\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Status prawny
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Quotation
- Co nie jest zabronione, jest dozwolone.
 Na pewno jednak nie możemy uznać bitcoina za prawny środek płatniczy.
 W świetle dyrektyw unijnych nie jest on też pieniądzem elektronicznym –
 powiedział Szymon Woźniak z Ministerstwa Finansów.
\end_layout

\begin_layout Quotation
Szymon Woźniak zaznaczył, że zyski z transakcji na bitcoinie podlegają w
 Polsce opodatkowaniu w taki sam sposób jak zyski z praw majątkowych.
 Osoby, które nie zgłoszą urzędowi skarbowemu dochodów z handlu bitcoinami,
 mogą się liczyć z restrykcjami, podkreślił.
\end_layout

\begin_layout Quotation
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Quotation
http://www.pb.pl/3485125,94998,minfin-bitcoin-nie-jest-nielegalny
\end_layout

\end_deeper
\begin_layout Section
Krytyka
\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Krytyka
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Obawy dotyczące Bitcoin to np.:
\end_layout

\begin_layout Itemize
gwałtowne zmniejszenie ilości użytkowników
\end_layout

\begin_layout Itemize
delegalizacja przez rządy państw
\end_layout

\begin_layout Itemize
deflacja powodująca niechęć do sprzedawania
\end_layout

\begin_layout Itemize
ataki nieetycznych hakerów na giełdy i portfele
\end_layout

\begin_layout Itemize
nie jest do końca anonimowy (można na podstawie przelewów z banków wnioskować
 kto zakupił dane BTC na giełdzie)
\end_layout

\begin_layout Itemize
nieodwracalność transakcji uniemożliwia naprawę pomyłek
\end_layout

\end_deeper
\begin_layout Section
Ostatnie incydenty
\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Ostatnie incydenty
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Mt.Gox, który kiedyś był największą giełdą wymiany bitmonet, wstrzymał na
 blisko miesiąc wszystkie wypłaty z powodu „kwestii technicznych”, po czym
 został zamknięty.
 Mt.Gox przyznał się do straty 750 000 bitcoinów należących do klientów oraz
 100 000 należących do samej spółki.
 Przy obecnym kursie bitcoiny te warte byłyby ok.
 480 milionów dolarów.
 Poszkodowanych zostało 127 tys.
 klientów.
 Za swoje problemy Mt.Gox wini atak hakerski oraz luki w systemie informatycznym.
\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Ostatnie incydenty cd.
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
W marcu 2014 r.
 doszło do włamania na Bitcurex.
 Włamywacz miał możliwość manipulacji saldami kont w złotówkach.
 Osoba ta skupiła bitcoiny po zawyżonym kursie – sięgającym nawet 20 000zł
 – których łączna ilość mogła wynieść nawet 19 000 BTC.
 Giełda bardzo szybko po odnotowaniu ataku została wyłączona, a w blockchanie
 nie zauważono dużych przepływów środków co wskazuje na to, że bitcoiny
 pozostały bezpieczne na portfelach giełdy.
 Przedstawiciele giełdy potwierdzili fakt włamania i w komunikacie na Facebooku
 zapewnili, że bitcoiny są bezpieczne.
\end_layout

\end_deeper
\begin_layout Section
Najpopularniejsze alternatywy dla Bitcoin
\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Najpopularniejsze alternatywy dla Bitcoin
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Enumerate
Litecoin $ 11.41 
\end_layout

\begin_layout Enumerate
Auroracoin $ 1.67 
\end_layout

\begin_layout Enumerate
Dogecoin $ 0.00046
\end_layout

\end_deeper
\begin_layout Section*
Podsumowanie
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Ciekawostki
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
W 2010r programista Laszlo Hanyecz chcąc udowodnić wartość nabywczą pierwszej
 kryptowaluty, za 10 000 bitcoinów zakupił dwie pizze.
 Dzisiaj to równowartość 18 265 785 zł.
\end_layout

\end_deeper
\begin_layout Frame

\end_layout

\begin_layout Frame
Bitcoin jest eksperymentalną walutą która jest aktywnie rozwijana, Mimo,
 że z każdą kolejną chwilą gdy jego użytkowanie rośnie staje się mniej eksperyme
ntalny to jednak pamiętać, że Bitcoin jest nowym wynalazkiem który eksploruje
 ideę która nigdy wcześniej nie została wypróbowana.
 Jako taki jego przyszłość nie może być przewidziana przez nikogo.
\end_layout

\begin_layout Frame

\end_layout

\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Ciekawe linki
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Spis ciekawych linków dotyczących BTC można znaleźć pod adresem: http://bitcoin.p
l/linki
\end_layout

\end_deeper
\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Bibliografia
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-11"

\end_inset

http://pl.wikipedia.org/wiki/Bitcoin
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-18"

\end_inset

http://pl.wikipedia.org/wiki/Kryptowaluta
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-19"

\end_inset

http://en.wikipedia.org/wiki/Bitcoin
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-36"

\end_inset

http://en.wikipedia.org/wiki/Cryptocurrency
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-35"

\end_inset

http://en.wikipedia.org/wiki/Ripple_%28payment_protocol%29
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-34"

\end_inset

http://en.wikipedia.org/wiki/Bitcoin_protocol
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-33"

\end_inset

http://bitcoin.org/pl/jak-to-dziala
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-32"

\end_inset

http://bitcoin.org/pl/o
\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Bibliografia
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-31"

\end_inset

http://bitcoin.org/pl/zabezpiecz-swoj-portfel
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-30"

\end_inset

http://bitcoin.org/pl/co-potrzebujesz-wiedziec
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-29"

\end_inset

http://bitcoin.org/pl/slownik
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-28"

\end_inset

http://www.investopedia.com/terms/b/bitcoin.asp
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-27"

\end_inset

http://coinmarketcap.com/
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-26"

\end_inset

http://www.coinchoose.com/
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-25"

\end_inset

http://www.pb.pl/3485125,94998,minfin-bitcoin-nie-jest-nielegalny
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-24"

\end_inset

https://polmine.pl/?action=faq
\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Bibliografia
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-23"

\end_inset

http://www.jtz.pl/jak-kopac-bitcoiny
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-22"

\end_inset

http://webhosting.pl/Bitcoin.bez.placzu.czesc.1.Jak.dziala.kryptograficzna.e_waluta
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-21"

\end_inset

http://ittechblog.pl/2011/06/19/bitcoin-wirtualna-waluta-przyszlosci/
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-20"

\end_inset

http://bitcoincharts.com/markets/
\end_layout

\end_deeper
\end_body
\end_document
